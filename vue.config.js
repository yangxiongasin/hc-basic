module.exports = {
    // 修改 src 目录 为 examples 目录
    pages: {
        index: {
            entry: 'examples/main.js',
            template: 'public/index.html',
            filename: 'index.html'
        }
    },
    lintOnSave: true,
    productionSourceMap: false,
    // 扩展 webpack 配置，使 packages 加入编译
    chainWebpack: config => {
        const jsRule = config.module.rule('js');
        jsRule.include
            .add('/packages')
            .end()
            .use('babel')
            .loader('babel-loader')
            .tap(options => {
                // 修改它的选项...
                return options;
            });
    },
    devServer: {
        port: '8080',
        disableHostCheck: true,
        https: false,
        hotOnly: false,
        //代理
        proxy: {
            '/admin/*': {
                target: 'http://192.168.0.109:8082/', // 华明远程地址
                // target: 'http://121.36.53.143:3100/mock/27/', // YAPI
                // target: 'http://wuhm.free.svipss.top/',
                changeOrigin: true, //是否允许跨域
                secure: false
            }
        }
    }
};
