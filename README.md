# HC-basic

## 基于VUE + ElementUI 二次封装开发包
> https://www.npmjs.com/package/hc-basic

### 启动项目
> npm run dev / npm run serve

### 项目打包
> npm run lib

### 项目发布
> npm publish

### 代码格式化
> npm run eslint

### 文档地址
> 
[文档地址](http://114.115.150.172:8088/)

### 示例项目-单体
> https://codeup.aliyun.com/6180d6736746bc7c6cc8e78a/hc/sample-single.git

### 示例项目-微前端-主项目

> https://codeup.aliyun.com/6180d6736746bc7c6cc8e78a/hc/sample-more-main.git

### 示例项目-微前端-子项目
> https://codeup.aliyun.com/6180d6736746bc7c6cc8e78a/hc/sample-more-child.git

## 目录结构
```markdown
├── components              ------> 公共组件
│   ├── desc                ------> 详情组件
│   ├── dialog              ------> 弹窗组件
│   ├── form-item           ------> 表单组件
│   ├── icon                ------> 图标组件
│   ├── identify            ------> 验证码组件
│   ├── layout              ------> 布局组件
│   ├── pager               ------> 分页组件
│   ├── popover             ------> 气泡组件
│   ├── tab                 ------> 页签组件
│   ├── table               ------> 表格组件
│   └── theme               ------> 主题组件
├── directives              ------> 公共指令
├── filters                 ------> 公共过滤器
├── scss                    ------> 公共样式
└── utils                   ------> 公共工具
    ├── encrypt             ------> 加密解密
    ├── func                ------> 公共函数
    └── valid               ------> 公共验证正则
```
