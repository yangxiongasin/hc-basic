import funcSet from './func';
import Valid from './valid';
import encrypt from './encrypt';
import ElementUI from 'element-ui';
const setElement = Vue => {
    Vue.use(ElementUI);
    // ElementUI的全局函数注册到Vue到全局
    Vue.prototype.$messge = ElementUI.Message;
    Vue.prototype.$msgbox = ElementUI.MessageBox;
    Vue.prototype.$alert = ElementUI.MessageBox.alert;
    Vue.prototype.$confirm = ElementUI.MessageBox.confirm;
    Vue.prototype.$prompt = ElementUI.MessageBox.prompt;
    Vue.prototype.$notify = ElementUI.Notification;
};

const setEncrypt = (Vue, options) => {
    const AES = encrypt.AES.encryptInit(options.Encrypt.AES.KEY, options.Encrypt.AES.IV);
    Vue.prototype.$AES = {
        encrypt: AES.Encrypt,
        decrypt: AES.Decrypt
    };
    // const RSA = encrypt.RSA.encryptInit(options.Encrypt.RSA.publicKey, options.Encrypt.privateKey);
    // Vue.prototype.$RSA = {
    //     encrypt: RSA.Encrypt,
    //     decrypt: RSA.Decrypt,
    //     signature: RSA.Signature,
    //     verify: RSA.Verify,
    // };
};
export { funcSet, Valid, encrypt, setElement, setEncrypt };
