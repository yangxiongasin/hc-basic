import CryptoJS from 'crypto-js/crypto-js';

function Init(KEYs, IVs) {
    // 默认的 KEY 与 iv 如果没有给
    const KEY = KEYs || '1234567812345678'; //""中与后台一样  密钥
    const IV = IVs || '1234567812345678'; //""中与后台一样  密钥偏移量

    /**
   * AES加密 ：字符串 key iv  返回base64
   */
    const Encrypt = (word, keyStr = KEY, ivStr = IV) => {
        let key = CryptoJS.enc.Utf8.parse(keyStr);
        let iv = CryptoJS.enc.Utf8.parse(ivStr);

        let srcs = CryptoJS.enc.Utf8.parse(word);
        let encrypted = CryptoJS.AES.encrypt(srcs, key, {
            iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
    };

    /**
   * AES 解密 ：字符串 key iv  返回base64
   *
   */
    const Decrypt = (word, keyStr = KEY, ivStr = IV) => {
        let key = CryptoJS.enc.Utf8.parse(keyStr);
        let iv = CryptoJS.enc.Utf8.parse(ivStr);

        let src = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Base64.parse(word));

        const decrypt = CryptoJS.AES.decrypt(src, key, {
            iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return decrypt.toString(CryptoJS.enc.Utf8);
    };
    return {
        Encrypt,
        Decrypt
    };
}

function randomString(len = 16) {
    let $chars = 'ABCDEFabcdef0123456789';
    let maxPos = $chars.length; //获取$char的长度
    let pwd = ''; //初始化密码
    for (let i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}

export default {
    encryptInit: Init
};
