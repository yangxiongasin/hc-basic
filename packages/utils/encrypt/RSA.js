import JsEncrypt from 'jsencrypt';

function Init(publicKey, privateKey) {
    const Encrypt = word => {
    //设置公钥
        JsEncrypt.setPublicKey(publicKey);
        return JsEncrypt.encrypt(word);
    };

    const Decrypt = word => {
    // 私钥
        JsEncrypt.setPrivateKey(privateKey);
        return JsEncrypt.decrypt(word);
    };
    const Signature = signData => {
    // 私钥加签
        let signPrivateKey = `-----BEGIN PRIVATE KEY-----${privateKey}-----END PRIVATE KEY-----`;
        let sig = new KJUR.crypto.Signature({ alg: 'SHA1withRSA', prov: 'cryptojs/jsrsa', prvkeypem: signPrivateKey });
        // let rsa = new KJUR()
        // rsa = KEYUTIL.getKey(privateKey)
        var hashAlg = 'sha1'; // 设置sha1
        var sign = sig.signString(signData, hashAlg); // 加签
        sign = hex2b64(sign);
        return sign;
    };
    // 验签 用公钥对签名进行验签
    const Verify = (signData, data) => {
    // signData: 加签的数据
    // data: 加签之后得到的签文
        try {
            let signPublicKey = `-----BEGIN PUBLIC KEY-----${publicKey}-----END PUBLIC KEY-----`;
            let sig = new KJUR.crypto.Signature({ alg: 'SHA1withRSA', prov: 'cryptojs/jsrsa', prvkeypem: signPublicKey });
            sig.updateString(signData);
            let result = sig.verify(data);
            return result;
        } catch (e) {}
    };
    return {
        Encrypt,
        Decrypt,
        Signature,
        Verify
    };
}
export default {
    encryptInit: Init
};
