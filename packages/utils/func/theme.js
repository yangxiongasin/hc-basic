const setTheme = ({ functional = {}, text = {}, border = {}, other = {}, shadow }) => {
    validParams(functional, 'functional');
    validParams(text, 'text');
    validParams(border, 'border');
    const functionalValue = { ...{ primary: '#409eff', success: '#67c23a', info: '#909399', warning: '#e6a23c', danger: '#f56c6c' }, ...functional };
    const textValue = { ...{ primary: '#303133', regular: '#606266', secondary: '#909399', placeholder: '#c0c4cc' }, ...text };
    const borderValue = { ...{ base: '#dcdfe6', light: '#e4e7ed', lighter: '#ebeef5', extraLight: '#f2f6fc' }, ...border };
    const otherValue = { ...other };
    const shadowValue = shadow || '#000000';
    const themeStyleResult = {};
    setFunctional(functionalValue, themeStyleResult);
    setColor(textValue, 'text', themeStyleResult);
    setColor(borderValue, 'border', themeStyleResult);
    setColor(otherValue, 'other', themeStyleResult);
    setShadow(shadowValue, themeStyleResult);
    addStyle(themeStyleResult);
};
const validParams = (data, type) => {
    const validValue = {
        functional: ['primary', 'success', 'info', 'warning', 'danger'],
        text: ['primary', 'regular', 'secondary', 'placeholder', 'danger'],
        border: ['base', 'light', 'lighter', 'extraLight']
    };
    for (let key in data) {
        if (!validValue[type].includes(key)) {
            throw `Function setTheme: 参数${type}错误，不在'${validValue[type].join('、')}'范围内`;
        }
    }
    return true;
};
const addStyle = data => {
    document.getElementById('hc-theme-style') && document.getElementById('hc-theme-style').remove();
    const style = document.createElement('style');
    style.type = 'text/css';
    style.id = 'hc-theme-style';
    let innerHTML = 'body{';
    for (let key in data) {
        innerHTML += `${key}:${data[key]};`;
    }
    style.innerHTML = innerHTML + '}';
    window.document.head.insertBefore(style, window.document.head.childNodes[0]);
};
const setFunctional = (data, result) => {
    for (let key in data) {
        const transList = [100, 95, 90, 85, 80, 50, 40, 20, 10, 5];
        transList.forEach(e => {
            const name = `--color-${key}${e === 100 ? '' : '-' + e}`;
            result[name] = mixs(data[key], e);
        });
    }
};
const setColor = (data, type = 'text', result) => {
    for (let key in data) {
        const name = `--color-${type}-${key}`;
        result[name] = data[key];
    }
};
const setShadow = (data, result) => {
    const transList = [90, 80, 70, 60, 50, 40, 30, 20, 10, 5];
    transList.forEach(e => {
        const name = `--shadow-${e}`;
        result[name] = mixs(data, e);
    });
};
const mixs = (color1, weight = 100) => {
    let color = 'rgb(';
    for (let i = 1; i <= 5; i += 2) {
        color += parseInt(color1.substr(i, 2), 16) + ',';
    }
    const rgbaVal = color + weight / 100 + ')';
    return hexify(rgbaVal);
};
const hexify = color => {
    let values = color
        .replace(/rgba?\(/, '')
        .replace(/\)/, '')
        .replace(/[\s+]/g, '')
        .split(',');
    let a = parseFloat(values[3] || 1),
        r = Math.floor(a * parseInt(values[0]) + (1 - a) * 255),
        g = Math.floor(a * parseInt(values[1]) + (1 - a) * 255),
        b = Math.floor(a * parseInt(values[2]) + (1 - a) * 255);
    return '#' + ('0' + r.toString(16)).slice(-2) + ('0' + g.toString(16)).slice(-2) + ('0' + b.toString(16)).slice(-2);
};

export default {
    setTheme,
    colorMix: mixs
};
