// 深度克隆
import Obj from './obj';

const cloneDeep = target => JSON.parse(JSON.stringify(target));

//排除数组某些项
const filterOptionArray = (arr, keys) => {
    return arr.map(e => {
        const newArrOne = {};
        Object.keys(e).forEach(key => {
            !keys.split(',').includes(key) && (newArrOne[key] = e[key]);
        });
        return newArrOne;
    });
};

// 数组扁平化，避免用flat；flat的IE11不兼容和谷歌版本52或以下不兼容
const flatten = arr => {
    while (arr.some(item => Array.isArray(item))) {
        arr = [].concat(...arr);
    }
    return arr;
};

// 统计元素在数组中出现的次数
const getEleCount = (obj, ele) => {
    const count = {};
    if (isArray(ele)) {
        ele.forEach(es => {
            count[es] = obj.reduce((total, e) => (es === e ? total + 1 : total + 0), 0);
        });
        return count;
    }
    return obj.reduce((total, e) => (ele === e ? total + 1 : total + 0), 0);
};

// 判断是否为数组
const isArray = arr => {
    return Object.prototype.toString.call(arr) === '[object Array]';
};

// 判断数组是否为[]
const isEmptyArray = arr => {
    return Object.prototype.toString.call(arr) === '[object Array]' && arr.length === 0;
};

// 判断数组是否相等
const isEqual = (arr1, arr2) => {
    return arr1.length === arr2.length && !arr1.find((e, i) => JSON.stringify(e) !== JSON.stringify(arr2[i]));
};

// 数组中对象元素去重，第一个参数为数组，第二个为去重对象的key
const repeatObjEle = (arr, key) => {
    const [result, hash] = [[], {}];
    for (let i = 0; i < arr.length; i++) {
        const elem = arr[i][key];
        if (!hash[elem]) {
            result.push(arr[i]);
            hash[elem] = true;
        }
    }
    return result;
};

// 去掉两个数组的相同项
const unSet = (arr1, arr2) => {
    // eslint-disable-next-line no-undef
    const [set1, set2, subset] = [new Set(arr1), new Set(arr2), []];
    for (let item of set1) {
        if (!set2.has(item)) {
            subset.push(item);
        } else {
            set1.delete(item);
            set2.delete(item);
        }
    }
    for (let item of set2) {
        if (!set1.has(item)) {
            subset.push(item);
        }
    }
    return subset;
};
const listToTree = (list, config = { id: 'id', parentId: 'parentId', children: 'children' }) => {
    const configs = Obj.deepMerge({ id: 'id', parentId: 'parentId', children: 'children' }, config);
    const menuObj = {},
        emptyEnum = ['', undefined, null];
    const { id, parentId, children } = configs;
    list.forEach(item => {
        item[children] = [];
        menuObj[item[id]] = item;
    });
    return list.filter(item => {
        if (!emptyEnum.includes(item[parentId]) && menuObj[item[parentId]]) {
            menuObj[item[parentId]][children].push(item);
            return false;
        }
        return true;
    });
};

const treeToList = (list, config = { id: 'id', parentId: 'parentId', children: 'children' }) => {
    const configs = Obj.deepMerge({ id: 'id', parentId: 'parentId', children: 'children' }, config);
    let result = []; // 用于存储递归结果（扁平数据）
    // 递归函数
    let fn = source => {
        source.forEach(item => {
            result.push(item);
            item[configs.children] && item[configs.children].length > 0 ? fn(item[configs.children]) : ''; // 子级递归
        });
    };
    fn(list);
    return result;
};

export default {
    cloneDeep,
    filterOptionArray,
    flatten,
    getEleCount,
    isArray,
    isEmptyArray,
    isEqual,
    repeatObjEle,
    listToTree,
    treeToList,
    unSet
};
