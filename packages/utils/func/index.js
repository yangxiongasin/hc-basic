import arrFunc from './arr';
import browserCacheFunc from './browserCache';
import dateFunc from './date';
import objFunc from './obj';
import themeFunc from './theme';
export default { ...arrFunc, ...browserCacheFunc, ...dateFunc, ...objFunc, ...themeFunc };
