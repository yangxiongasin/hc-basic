import desc from './desc';
import dialog from './dialog';
import drawer from './drawer';
import formItem from './form-item';
import identify from './identify';
import pager from './pager';
import popover from './popover';
import gant from './gant';
import tab from './tab';
import table from './table';
import frame from './frame';
import upload from './upload';
import tree from './tree';
import icon from './icon';

const components = {
    desc,
    dialog,
    'form-item': formItem,
    identify,
    frame,
    pager,
    gant,
    popover,
    tab,
    table,
    drawer,
    tree,
    upload,
    icon
};
// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install = function (Vue) {
    // 判断是否安装
    if (install.installed) return;
    // 遍历注册全局组件
    Object.keys(components).map(e => Vue.component('hc-' + e, components[e]));
};

// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}

export default {
    // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
    install
};
