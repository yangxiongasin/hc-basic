import Vue from 'vue';
import $Func from '../../utils/func';
const store = Vue.observable({
    tabData: [],
    menuData: [],
    menuActive: '',
    tabsActive: '',
    menuConfig: { label: 'label', icon: 'icon', children: 'children', value: 'value', parent: 'parent', fullPath: 'fullPath' }
});

const muta = {
    setTabs(name, childName) {
        const tabValueList = store.tabData.map(e => e[store.menuConfig.value]);
        if (childName ? !tabValueList.includes(childName) : !tabValueList.includes(name)) {
            const currentItem = store.menuData.find(e => {
                return e[store.menuConfig.value] === (childName || name);
            });
            currentItem && store.tabData.push(currentItem);
        } else {
            const index = store.tabData.findIndex(e => e[store.menuConfig.value] === childName);
            const currentItem = store.menuData.find(e => e[store.menuConfig.value] === (childName || name));
            index >= 0 && (store.tabData[index][store.menuConfig.fullPath] = currentItem[store.menuConfig.fullPath]);
            store.tabData = JSON.parse(JSON.stringify(store.tabData));
        }
        store.menuActive = name;
        store.tabsActive = childName || name;
    },
    deleteTabs(val, state) {
        if (store.tabData.length <= 1) return;
        const index = store.tabData.findIndex(e => e[store.menuConfig.value] === val);
        !state && this.setTabs(store.tabData[index ? index - 1 : index + 1][store.menuConfig.value]);
        store.tabData.splice(index, 1);
        return true;
    }
};
export { store, muta };
