const operationConfigValidator = params => {
    const labelNo = params.findIndex(e => !e.label);
    if (labelNo !== -1) {
        console.log(`%chc-tree => operationConfig参数：第${labelNo + 1}个参数label字段不能为空`, 'color: #e88282');
        return false;
    }
    const propNo = params.findIndex(e => !e.prop || !['add', 'edit', 'clean'].includes(e.prop));
    if (propNo !== -1) {
        console.log(`%chc-tree => operationConfig参数：第${propNo + 1}个参数prop字段不能为空且需要在'add'、'edit'、'clean'之中`, 'color: #e88282');
        return false;
    }
    const httpUrlNo = params.findIndex(e => !e.httpUrl);
    if (httpUrlNo !== -1) {
        console.log(`%chc-tree => operationConfig参数：第${httpUrlNo + 1}个参数httpUrl字段不能为空`, 'color: #e88282');
        return false;
    }
    return true;
};

const addEditValidator = params => {
    return true;
};
const operationModelValidator = params => {
    return ['dialog', 'drawer'].includes(params);
};
export { operationConfigValidator, addEditValidator, operationModelValidator };
