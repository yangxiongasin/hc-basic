// 内需插件： 深度合并、ELementUI
import deepmerge from 'deepmerge';

// 引入hc-basic包默认配置可被外部传入参数覆盖
import basicOptions from './options.js';

// 引入公共组件、指令、过滤器
import components from './components';
import directives from './directives';
import filters from './filters';
// 引入axios配置
import axiosHttp from './http';

// 引入公共函数（数组函数、对象函数、日期函数、cookies函数）、引入验证正则、加密解密方法
import { funcSet, Valid, setElement, setEncrypt } from './utils';

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册1
const install = (Vue, option = {}) => {
    // 判断是否安装
    if (install.installed) return;
    const options = deepmerge(basicOptions, option);
    Vue.prototype.$Config = options;

    setElement(Vue);
    // 遍历注册全局组件
    Vue.use(components);
    Vue.use(directives, options.directives);
    Vue.use(filters);
    Vue.prototype.$hcHttp = axiosHttp(options.axios);
    Vue.prototype.$Func = funcSet;
    Vue.prototype.$Valid = Valid;
    setEncrypt(Vue, options);
};

// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}

export default {
    // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
    install
};
