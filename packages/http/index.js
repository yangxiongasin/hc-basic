/**
 * @file 本文件为服务调用插件实现
 * @author Yang。
 */

import axios from 'axios';
import { Message } from 'element-ui';
import { funcSet } from '../utils';
/**
 * 深度合并多个对象，返回合并后的新对象
 * @private
 * @param  {...Object} objs 多个对象
 * @return {Object} 返回合并后的新对象，原对象内容不变
 */
const statusCode = {
    400: '错误请求',
    401: '未授权，请重新登录',
    403: '拒绝访问',
    404: '请求错误,未找到该资源',
    405: '请求方法未允许',
    408: '请求超时',
    500: '服务器端出错',
    501: '网络未实现',
    502: '网络错误',
    503: '服务不可用',
    504: '网络超时',
    505: 'http版本不支持该请求'
};

let showError = (errorMsg, alwaysDisplay) => Message.error(errorMsg || '服务调用出错');

const httpInit = (option = {}) => {
    const { config = {}, successCode, token, requestCallback, responseCallback } = option;
    const getInstances = conf => {
            let result = {};
            let opts = funcSet.deepMerge(axios.defaults, conf);
            for (let key in opts) {
                axios.defaults[key] = opts[key];
            }
            // axios.defaults.headers['Content-Type'] = 'application/json';
            result.$http = axios;
            return result;
        },
        requestInterceptor = config => {
            if (requestCallback && typeof requestCallback === 'function') {
                requestCallback(config);
            } else {
                // 将post方法的content-type 设置为 application/x-www-form-item-item-urlencoded
                config.headers.common.token = token;
                if (config.method === 'post') {
                    let data = config.data;
                    for (let key in data) {
                        if (data[key] === null) data[key] = '';
                    }
                }
            }
            return config;
        },
        requestError = error => Promise.reject(error),
        responseInterceptor = response => {
            if (responseCallback && typeof responseCallback === 'function') {
                return responseCallback(response);
            }
            return response.data;
        },
        responseError = error => {
            if (error && error.response) {
                showError(statusCode[error.response.status] || `连接错误${error.response.status}`);
                if ([401, 404].includes(error.response.status)) {
                    // window.location.href = error.response.status === '401' ? '/login' : '/NotFound';
                }
            } else {
                showError(JSON.stringify(error).includes('timeout') ? '服务器响应超时，请刷新当前页' : '连接服务器失败');
            }
            return Promise.reject(error);
        };
    axios.defaults.headers = Object.assign(axios.defaults.headers, config.headers);
    const http = getInstances(config);
    // 默认服务调用拦截器设置
    http.$http.interceptors.request.use(requestInterceptor, requestError);
    http.$http.interceptors.response.use(responseInterceptor, responseError);

    return axios;
};

export default httpInit;
