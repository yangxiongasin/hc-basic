export default {
    Components: {
        formItem: {
            labelFontSize: 14, // form项的label字体大小，用来设置label的宽度
            label: 'label', // 默认select、checkbox、radio的option的label字段
            value: 'value', // 默认select、checkbox、radio的option的value字段
            space: '20px' // 各子组件中间距的大小
        },
        pager: {
            currentPage: 'pageNo', // 当前页配置字段
            pageSize: 'pageSize', // pager组件page-size字段
            layout: 'total, prev, pager, next, sizes, jumper' // pager组件layout
        },
        table: {
            noData: 'https://frontendonline-erp.ocj.com.cn/img/nodata.png', // 没有数据展示图片，可通过require导入本地图片
            optionLabel: '操作', // table操作列名称
            imgWidth: '80px',
            imgHeight: '80px',
            imgDialogWidth: '1200px'
        },
        desc: {
            color: {
                label: 'regular', // 详情label字段颜色
                value: 'textPrimary' // 详情值颜色
            },
            emptyText: '------', // 详情单独对应字段为空的默认显示
            distance: '16', // 详情字段间距
            imgWidth: '80px',
            imgHeight: '80px',
            imgDialogWidth: '1200px'
        },
        drawer: {
            submit: '确定',
            back: '返回'
        }
    },
    tCode: {
        path: '/code', // 使用tcode组件时，tcode后台地址
        key: 'code', // 入参key字段
        type: 'post', // get post
        dataType: 'isNumber', // ''''
        isNumValue: 1, // 该字典类型的值,如果是1，那么就是数字类型。否则为字符串类型
        cache: true, // tocode 是否缓存不刷新数据
        func: res => res.data
    },
    Encrypt: {
        AES: {
            KEY: '', // aes加密-密钥
            IV: '' // aes加密-密钥偏移量
        }
    },
    directives: {
        copy: {},
        errImg: {
            avatarErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/avator.png', // 头像错误图片
            showErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/nofound.png', // 展示错误图片
            upErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/upload-fail.png' // 上传错误图片
        },
        onlyInt: {},
        dialogDrag: {},
        onlyNumber: {},
        watermark: {
            width: 0, // 水印宽度
            height: 0, // 水印高度
            fillStyle: 'rgba(180, 180, 180, 0)', // 文字颜色
            font: '16px Microsoft JhengHei', // 文本字体大小字体类型
            title: '', // 水印标题
            subTitle: '' // 水印副标题
        },
        rightMenu: {
            menuStyle: 'width:150px;background:#fff;box-shadow: 0 1px 5px 0 rgb(45 47 51 / 10%);cursor: pointer', // 右键菜单样式
            childMenuStyle: 'color:#000;line-height:30px;font-size:15px;border-bottom: 1px solid #ddd' // 右键子菜单样式
        },
        preventShake: {
            time: 2000 // 间隔时间
        }
    }
};
