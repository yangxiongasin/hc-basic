const watermark = val => {
    return {
        bind: (el, binding) => {
            function addWaterMarker(title, subTitle, parentNode, font, textColor) {
                // 水印文字，父元素，字体，文字颜色
                const can = document.createElement('canvas');
                parentNode.appendChild(can);
                can.width = val.width;
                can.height = val.height;
                can.style.display = 'none';
                const cans = can.getContext('2d');
                cans.rotate((-20 * Math.PI) / 180);
                cans.font = font;
                cans.fillStyle = textColor;
                cans.textAlign = 'left';
                cans.textBaseline = 'Middle';
                cans.fillText(title, can.width / 3, can.height / 2);
                cans.fillText(subTitle, can.width / 3, can.height / 2 + 20);
                parentNode.style.backgroundImage = 'url(' + can.toDataURL('image/png') + ')';
            }
            addWaterMarker(binding.value.title || val.title, binding.value.subTitle || val.subTitle, el, binding.value.font || val.font, binding.value.textColor || val.fillStyle);
        }
    };
};

export default watermark;
