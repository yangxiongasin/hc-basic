class Mask {
    constructor() {
        this.maskStyle = 'position:fixed;top:0;left:0;width:100%;height:100%;z-index:99997;';
    }
    add() {
        const Mask = document.createElement('div');
        Mask.style = this.maskStyle + 'display:none';
        Mask.setAttribute('id', 'hc-right-menu-mask');
        document.body.appendChild(Mask);
    }
    show() {
        document.getElementById('hc-right-menu-mask').style = this.maskStyle + 'display:block';
    }
    hide() {
        document.getElementById('hc-right-menu-mask').style = this.maskStyle + 'display:none';
    }
}
// 阻止默认行为
const stopBehaves = el => {
    el.stopPropagation(); //阻止冒泡事件
    el.cancelBubble = true; //阻止冒泡事件ie
    el.preventDefault(); //阻止默认事件
};
const rightMenu = val => {
    let rightMenuNum = 0;
    return {
    // 插入时候触发
        bind(el, binding, vnode) {
            if (!(binding.value instanceof Array)) {
                throw new Error('请设置右键菜单项目，数据类型为Array');
            }
            const Model = new Mask();
            // 存放当前右键菜单ID名称
            const currentDom = 'hc-right-menu' + rightMenuNum;
            // 设置body宽高（为了遮照）
            document.body.style.position = 'fixed';
            document.body.style.width = '100%';
            document.body.style.height = '100%';
            el.style.position = el.style.position || 'relative';
            el.style.zIndex = '99998';
            // 增加一个遮罩层方便我控制菜单显示时候取消其余事件
            Model.add();
            el.addEventListener('contextmenu', event => {
                const e = event || window.event;
                stopBehaves(e);
                // 菜单位置
                const menuX = e.pageX || e.pageY ? e.pageX : e.clientX + document.body.scrollLeft - document.body.clientLeft; //获取pageX 兼容ie
                const menuY = e.pageX || e.pageY ? e.pageY : e.clientY + document.body.scrollTop - document.body.clientTop;
                // 隐藏所有菜单
                for (let i = 0; i < rightMenuNum; i++) {
                    let rightMenuDom = document.getElementById('hc-right-menu' + i);
                    rightMenuDom && (rightMenuDom.style = `display:none`);
                }
                // 右键显示遮罩层
                Model.show();
                // 找不到这个节点时候 新增一个menu 用于多个菜单的兼容问题
                if (!document.getElementById(currentDom)) {
                    // 创建div
                    let menuContent = document.createElement('div');
                    menuContent.className = 'hcRightMenu';
                    // 指令的绑定值进行遍历 生成菜单的节点
                    (binding.value || []).map(item => {
                        let childMenu = document.createElement('div');
                        // 文字内容
                        let content = item.name || '空';
                        // 设置节点文字不可选中
                        childMenu.setAttribute('unselectable', 'on');
                        childMenu.innerHTML = content;
                        childMenu.className = 'hcRightMenuChild';
                        // 菜单点击事件
                        childMenu.onclick = function () {
                            // 隐藏菜单的父级节点
                            childMenu.parentNode.style.display = 'none';
                            Model.hide();
                            // 用户无点击事件异常处理
                            return item.clickFunc ? vnode.context[item.clickFunc](item) : false;
                        };
                        // 禁用右键默认行为
                        childMenu.addEventListener('contextmenu', event => {
                            const e = event || window.event;
                            stopBehaves(e);
                        });
                        // 追加选项到总菜单
                        menuContent.appendChild(childMenu);
                    });
                    // 设置唯一id
                    menuContent.setAttribute('id', currentDom);
                    // 菜单的样式
                    menuContent.style = `top:${menuY}px;left:${menuX}px;`;
                    //    追加到页面
                    document.body.appendChild(menuContent);
                } else {
                    // 节点已经存在则不需要重复创建 节省性能，只需要获取后设置位置即可
                    let menuContent = document.getElementById(currentDom);
                    menuContent.style = `top:${menuY}px;left:${menuX}px;`;
                    // 判断是否超出屏幕宽度
                    if (menuX + menuContent.clientWidth >= document.body.clientWidth) {
                        menuContent.style.left = menuX - menuContent.clientWidth + 'px';
                    }
                    // 判断是否超出屏幕高度
                    if (menuY + menuContent.clientHeight >= document.body.clientHeight) {
                        menuContent.style.top = menuY - menuContent.clientHeight + 'px';
                    }
                }
            });
            // 每次创建都会使得唯一遍量增加 防止重复
            rightMenuNum++;
            const eventCallback = () => {
                document.getElementById(currentDom) && (document.getElementById(currentDom).style = `display:none`);
                document.getElementById('hc-right-menu-mask').style = 'display:none';
            };
            // 增加遮罩层的点击事件 （在空白处点击移除右键菜单）-》包含左键和右键点击
            document.getElementById('hc-right-menu-mask').addEventListener('click', eventCallback);
            document.getElementById('hc-right-menu-mask').addEventListener('contextmenu', eventCallback);
        },
        unbind(el) {
            // 解绑时候移除右键监听防止影响其他页面
            el.removeEventListener('contextmenu', this, true);
        }
    };
};
export default rightMenu;
