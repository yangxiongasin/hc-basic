import Vue from 'vue';
const state = Vue.observable({
    tCodeData: {},
    textColor: {
        primary: 'color-primary',
        success: 'color-success',
        danger: 'color-danger',
        info: 'color-info',
        warning: 'color-warning',
        textPrimary: 'color-text-primary',
        regular: 'color-text-regular',
        secondary: 'color-text-secondary',
        placeholder: 'color-text-placeholder',
        white: 'color-text-white'
    }
});

const muta = {
    getListData(vm, value) {
    // 获取浏览器localStroge缓存数据tcodeListData
        const tCodeData = JSON.parse(window && window.localStorage.getItem('tCodeData')) || {};
        const tCode = vm.$Config.tCode;
        // 默认入参
        let params = { [tCode.key]: value };
        // 如果设置了缓存，那么判断state里面有没有该数据，如果没有那么进行数据请求
        if ((tCode.cache && !state.tCodeData[value]) || !tCode.cache) {
            // 设置get方式请求入参
            tCode.type === 'get' && (params = { params });
            vm.$hcHttp[tCode.type](vm.$Config.axios.ipUrl + tCode.path, params).then(res => {
                const result = vm.$Func.filterOptionArray(tCode.func(res) || [], 'id');
                // 后台返回字典值取数字或者字符串数据
                state.tCodeData[value] = result.map(e => {
                    let value = e[vm.$Config.Components.formItem.value];
                    if (tCode.dataType && e[tCode.dataType] === tCode.isNumValue) {
                        value = Number(value || '0') || 0;
                    }
                    return { ...e, [vm.$Config.Components.formItem.value]: value };
                });
                state.tCodeData = JSON.parse(JSON.stringify({ ...tCodeData, ...state.tCodeData }));
                vm.$Func.addStorgeValue('tCodeData', JSON.stringify(state.tCodeData));
            });
        }
    }
};

export { state, muta };
