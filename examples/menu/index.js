import patient from './patient'; // 患者管理
let websiteIndex = [
    {
        code: 'websiteIndex',
        name: '首页',
        icon: 'el-icon-s-home',
        parentKey: '',
        children: null
    }
];
export default [...websiteIndex, ...patient];
