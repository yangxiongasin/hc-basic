export default {
    Components: {
        formItem: {
            labelFontSize: 14, // form项的label字体大小，用来设置label的宽度
            label: 'label', // 默认select、checkbox、radio的option的label字段
            value: 'value' // 默认select、checkbox、radio的option的value字段
        },
        pager: {
            currentPage: 'pageNo', // 当前页配置字段
            pageSize: 'pageSize' // pager组件page-size字段
        },
        table: {
            optionLabel: '操作'
        },
        layout: {
            topHeight: 54, // 顶部内容高度
            bottomHeight: 0, // 菜单底部内容高度
            menuWidth: '246px', // 右侧菜单栏宽度
            closeMenuWidth: '61px' // 右侧菜单栏收起时
        }
    },
    tCode: {
        path: '/dict/data/getDirtDataList', // tcode请求地址
        key: 'type', // tcode入参字段
        type: 'post', // get post
        cache: true, // 是否缓存处理
        dataType: 'isNumber', // ''''
        isNumValue: 1, // '''
        func: res => res.data.data
    },
    Encrypt: {
        AES: {
            KEY: '1234567812345678', // aes加密-密钥
            IV: '1234567812345678' // aes加密-密钥偏移量
        }
    },
    directives: {
        copy: {},
        errImg: {
            avatarErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/avator.png', // 头像错误图片
            showErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/nofound.png', // 展示错误图片
            upErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/upload-fail.png' // 上传错误图片
        },
        onlyInt: {},
        onlyNumber: {},
        preventShake: {
            time: 500
        }
    },
    axios: {
        ipUrl: '',
        // axios基础配置
        config: {
            baseURL: 'http://121.36.53.143:3100/mock/27/admin/global', //联调
            timeout: 60000,
            retry: 1, // 超时失败时重连一次
            retryDelay: 1000,
            withCredentials: true, // default
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            }
        },
        // 后端定义返回成功参数code
        successCode: undefined,
        // token参数
        token: '',
        // 请求拦截器
        requestCallback: config => {
            config.headers.token = localStorage.getItem('token');
            config.headers['Content-Type'] = 'application/json';
            return config;
        }
    }
};
