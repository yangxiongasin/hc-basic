export default {
    treeData1: [
        {
            menuId: 1,
            menuName: '系统管理',
            childrenList: [
                {
                    menuId: 100,
                    menuName: '用户管理',
                    childrenList: [
                        {
                            menuId: 1000,
                            childrenList: [
                                {
                                    menuId: 10001,
                                    childrenList: [],
                                    menuName: '用户查询1'
                                }
                            ],
                            menuName: '用户查询'
                        },
                        {
                            menuId: 1001,
                            childrenList: [],
                            menuName: '用户新增'
                        },
                        {
                            menuId: 1002,
                            childrenList: [],

                            menuName: '用户修改'
                        },
                        {
                            menuId: 1003,
                            menuName: '用户删除',
                            childrenList: [],
                            leaf: true
                        }
                    ]
                },
                {
                    menuId: 101,
                    menuName: '角色管理',
                    childrenList: [
                        {
                            menuId: 1006,
                            menuName: '角色查询',
                            childrenList: [],
                            leaf: true
                        },
                        {
                            menuId: 1007,
                            menuName: '角色新增'
                        },
                        {
                            menuId: 1008,
                            menuName: '角色修改'
                        },
                        {
                            menuId: 1011,
                            menuName: '删除角色'
                        }
                    ]
                }
            ]
        }
    ],
    treeData2: [
        {
            parentId: '0',
            id: 'A',
            label: 'label-A',
            children: [
                {
                    parentId: 'A',
                    id: 'A-1',
                    label: 'label-A-1'
                }
            ]
        },
        {
            parentId: '0',
            value: 'B',
            label: 'label-B',
            children: []
        }
    ]
};
