import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/index.vue';
Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        children: [
            { path: '/system/userManage', name: 'userManage', meta: { title: '用户管理' }, component: () => import('../views/system/userManage') },
            { path: '/system/dictManage', name: 'dictManage', meta: { title: '字典管理' }, component: () => import('../views/system/dictManage') },
            { path: '/system/temp', name: 'temp', meta: { title: '字典管理' }, component: () => import('../views/system/temp') },
            { path: '/system/dictManage/details', name: 'dictManageDetails', meta: { title: '字典管理详情', parent: 'dictManage' }, component: () => import('../views/system/dictManage/details') },
            { path: '/system/dictManage/demo', name: 'dictManageDemo', meta: { title: '字典管理详情', parent: 'dictManage' }, component: () => import('../views/system/dictManage/demo') },
            { path: '/system/userManage/details', name: 'userManageDetails', meta: { title: '用户管理详情', parent: 'userManage' }, component: () => import('../views/system/userManage/details') }
        ]
    },
    {
        path: '/demo',
        name: 'demo',
        component: () => import('../views/demo')
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router;
