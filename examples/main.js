import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import components from '../packages/index';
import options from './options.js';
import './assets/styles/index.scss';
Vue.config.productionTip = false;
components.install(Vue, options);
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
