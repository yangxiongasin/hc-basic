export default [
    {
        code: 'systemManage',
        name: '系统管理',
        icon: 'el-icon-s-home',
        parentKey: '',
        children: [
            {
                code: 'institutions',
                name: '机构管理',
                parentKey: 'systemManage'
            },
            {
                code: 'institutions2',
                name: '机构管理2',
                parentKey: 'systemManage'
            }
        ]
    }
];
